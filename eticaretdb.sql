USE [eticaret]
GO
/****** Object:  Table [dbo].[adresler]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[adresler](
	[adresID] [int] IDENTITY(1,1) NOT NULL,
	[uyeID] [int] NOT NULL,
	[adresTanimi] [text] NULL,
	[ad] [nvarchar](250) NOT NULL,
	[soyad] [nvarchar](250) NOT NULL,
	[firma] [nvarchar](250) NULL,
	[sehirID] [int] NOT NULL,
	[ilceID] [int] NOT NULL,
	[Adres] [text] NOT NULL,
	[TCno] [text] NOT NULL,
	[cepTel] [text] NULL,
	[gecicimi] [bit] NULL,
 CONSTRAINT [PK_adresler] PRIMARY KEY CLUSTERED 
(
	[adresID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fiyatAlarm]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fiyatAlarm](
	[fiyatAlarmID] [int] IDENTITY(1,1) NOT NULL,
	[uyeID] [int] NOT NULL,
	[urunID] [int] NOT NULL,
	[mevcutfiyat] [float] NOT NULL,
 CONSTRAINT [PK_fiyatAlarm] PRIMARY KEY CLUSTERED 
(
	[fiyatAlarmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[il]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[il](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ilKodu] [int] NULL,
	[ilAdi] [nvarchar](255) NULL,
 CONSTRAINT [PK_il] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ilce]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ilce](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ilKodu] [int] NULL,
	[ilceKodu] [int] NULL,
	[ilceAdi] [nvarchar](255) NULL,
 CONSTRAINT [PK_ilce] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[kategoriler]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[kategoriler](
	[kategoriID] [int] IDENTITY(1,1) NOT NULL,
	[anakategoriID] [int] NULL,
	[kategoriAdi] [nvarchar](100) NOT NULL,
	[guvenliurl] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_kategoriler] PRIMARY KEY CLUSTERED 
(
	[kategoriID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[kategoriOzellikleri]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[kategoriOzellikleri](
	[kategoriOzellikleriID] [int] IDENTITY(1,1) NOT NULL,
	[kategoriID] [int] NOT NULL,
	[ozellikAdi] [nvarchar](50) NOT NULL,
	[guvenliurl] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_kategoriOzellikleri] PRIMARY KEY CLUSTERED 
(
	[kategoriOzellikleriID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[markalar]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[markalar](
	[markaID] [int] IDENTITY(1,1) NOT NULL,
	[markaAd] [text] NOT NULL,
 CONSTRAINT [PK_markalar] PRIMARY KEY CLUSTERED 
(
	[markaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sepet]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sepet](
	[sepetID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[uyeID] [int] NOT NULL,
 CONSTRAINT [PK_sepet] PRIMARY KEY CLUSTERED 
(
	[sepetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[siparisler]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[siparisler](
	[siparisbilgileriID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[saticiID] [int] NOT NULL,
	[aliciID] [int] NOT NULL,
	[siparisSaticiOnayi] [bit] NULL,
	[siparisAliciOnayi] [bit] NULL,
	[siparisDurumu] [nvarchar](500) NULL,
	[siparisTarihi] [datetime] NOT NULL,
	[siparisKontrol] [bit] NULL,
	[siparisTutari] [float] NOT NULL,
	[aliciNotu] [text] NULL,
 CONSTRAINT [PK_siparisler] PRIMARY KEY CLUSTERED 
(
	[siparisbilgileriID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[urunKampanyalar]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[urunKampanyalar](
	[urunKampanyaID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[indirimOrani] [int] NULL,
	[baslangic] [datetime] NOT NULL,
	[bitis] [datetime] NOT NULL,
	[kampanyaBasigi] [text] NOT NULL,
	[kampanyaAciklamasi] [text] NULL,
	[kampantaGorseli] [text] NULL,
 CONSTRAINT [PK_urunKampanyalar] PRIMARY KEY CLUSTERED 
(
	[urunKampanyaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[urunler]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[urunler](
	[urunID] [int] IDENTITY(1,1) NOT NULL,
	[kategoriID] [int] NOT NULL,
	[markaID] [int] NOT NULL,
	[saticiID] [int] NOT NULL,
	[baslik] [text] NOT NULL,
	[detay] [text] NOT NULL,
	[urunAnaResim] [text] NULL,
	[yeniFiyat] [float] NOT NULL,
	[eskiFiyat] [float] NULL,
	[stok] [int] NOT NULL,
	[satilan] [int] NULL,
	[eklenmetarihi] [datetime] NOT NULL,
 CONSTRAINT [PK_urunler] PRIMARY KEY CLUSTERED 
(
	[urunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[urunOzellikleri]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[urunOzellikleri](
	[urunOzelliklerID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[kategoriOzellikleriID] [int] NOT NULL,
	[ozellikDetay] [text] NULL,
 CONSTRAINT [PK_urunOzellikleri] PRIMARY KEY CLUSTERED 
(
	[urunOzelliklerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[urunResimler]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[urunResimler](
	[urunResimlerID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[resimUrl] [text] NOT NULL,
 CONSTRAINT [PK_urunResimler] PRIMARY KEY CLUSTERED 
(
	[urunResimlerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[uyeler]    Script Date: 15.10.2016 23:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[uyeler](
	[uyeID] [int] IDENTITY(1,1) NOT NULL,
	[uyeAdSoyad] [text] NOT NULL,
	[kullaniciAdi] [nvarchar](250) NOT NULL,
	[sifre] [nvarchar](250) NOT NULL,
	[telefonNo] [text] NULL,
	[ceptelno] [text] NULL,
	[profilFoto] [text] NULL,
	[bakiye] [float] NOT NULL,
	[uyeRol] [nvarchar](50) NOT NULL,
	[cinsiyet] [nchar](10) NOT NULL,
	[dogumTarihi] [date] NOT NULL,
	[TCno] [nvarchar](12) NOT NULL,
	[eposta] [nvarchar](500) NOT NULL,
	[guvenlikSorusu] [text] NOT NULL,
	[guvenlikSorusuCevabi] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_uyeler] PRIMARY KEY CLUSTERED 
(
	[uyeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[il] ON 

INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (1, 1, N'Adana')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (2, 2, N'Adıyaman')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (3, 3, N'Afyon')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (4, 4, N'Ağrı')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (5, 5, N'Amasya')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (6, 6, N'Ankara')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (7, 7, N'Antalya')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (8, 8, N'Artvin')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (9, 9, N'Aydın')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (10, 10, N'Balıkesir')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (11, 11, N'Bilecik')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (12, 12, N'Bingöl')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (13, 13, N'Bitlis')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (14, 14, N'Bolu')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (15, 15, N'Burdur')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (16, 16, N'Bursa')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (17, 17, N'Çanakkale')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (18, 18, N'Çankırı')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (19, 19, N'Çorum')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (20, 20, N'Denizli')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (21, 21, N'Diyarbakır')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (22, 22, N'Edirne')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (23, 23, N'Elazığ')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (24, 24, N'Erzincan')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (25, 25, N'Erzurum')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (26, 26, N'Eskişehir')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (27, 27, N'Gaziantep')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (28, 28, N'Giresun')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (29, 29, N'Gümüşhane')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (30, 30, N'Hakkari')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (31, 31, N'Hatay')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (32, 32, N'Isparta')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (33, 33, N'İ&#231;el')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (34, 34, N'İstanbul')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (35, 35, N'İzmir')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (36, 36, N'Kars')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (37, 37, N'Kastamonu')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (38, 38, N'Kayseri')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (39, 39, N'Kırklareli')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (40, 40, N'Kırşehir')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (41, 41, N'Kocaeli')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (42, 42, N'Konya')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (43, 43, N'Kütahya')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (44, 44, N'Malatya')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (45, 45, N'Manisa')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (46, 46, N'K.Maraş')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (47, 47, N'Mardin')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (48, 48, N'Muğla')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (49, 49, N'Muş')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (50, 50, N'Nevşehir')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (51, 51, N'Niğde')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (52, 52, N'Ordu')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (53, 53, N'Rize')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (54, 54, N'Sakarya')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (55, 55, N'Samsun')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (56, 56, N'Siirt')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (57, 57, N'Sinop')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (58, 58, N'Sivas')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (59, 59, N'Tekirdağ')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (60, 60, N'Tokat')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (61, 61, N'Trabzon')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (62, 62, N'Tunceli')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (63, 63, N'Şanlıurfa')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (64, 64, N'Uşak')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (65, 65, N'Van')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (66, 66, N'Yozgat')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (67, 67, N'Zonguldak')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (68, 68, N'Aksaray')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (69, 69, N'Bayburt')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (70, 70, N'Karaman')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (71, 71, N'Kırıkkale')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (72, 72, N'Batman')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (73, 73, N'Şırnak')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (74, 74, N'Bartın')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (75, 75, N'Ardahan')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (76, 76, N'Iğdır')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (77, 77, N'Yalova')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (78, 78, N'Karabük')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (79, 79, N'Kilis')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (80, 80, N'Osmaniye')
INSERT [dbo].[il] ([ID], [ilKodu], [ilAdi]) VALUES (81, 81, N'Düzce')
SET IDENTITY_INSERT [dbo].[il] OFF
SET IDENTITY_INSERT [dbo].[ilce] ON 

INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (1, 1, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (2, 1, 20, N'ALADAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (3, 1, 28, N'CEYHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (4, 1, 36, N'FEKE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (5, 1, 40, N'IMAMOGLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (6, 1, 48, N'KARAISALI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (7, 1, 52, N'KARATAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (8, 1, 56, N'KOZAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (9, 1, 64, N'POZANTI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (10, 1, 68, N'SAIMBEYLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (11, 1, 72, N'SEYHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (12, 1, 76, N'TUFANBEYLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (13, 1, 80, N'YUMURTALIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (14, 1, 84, N'YÜREGIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (15, 2, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (16, 2, 20, N'BESNI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (17, 2, 25, N'ÇELIKHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (18, 2, 30, N'GERGER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (19, 2, 35, N'GÖLBASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (20, 2, 40, N'KAHTA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (21, 2, 45, N'SAMSAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (22, 2, 50, N'SINCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (23, 2, 55, N'TUT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (24, 3, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (25, 3, 20, N'BASMAKÇI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (26, 3, 24, N'BAYAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (27, 3, 28, N'BOLVADIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (28, 3, 32, N'ÇOBANLAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (29, 3, 36, N'ÇAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (30, 3, 40, N'DAZKIRI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (31, 3, 44, N'DINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (32, 3, 48, N'EMIRDAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (33, 3, 52, N'EVCILER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (34, 3, 56, N'HOCALAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (35, 3, 60, N'IHSANIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (36, 3, 64, N'ISCEHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (37, 3, 68, N'KIZILÖREN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (38, 3, 72, N'SANDIKLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (39, 3, 76, N'SINCANLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (40, 3, 80, N'SULTANDAGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (41, 3, 84, N'SUHUT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (42, 4, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (43, 4, 20, N'DIYADIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (44, 4, 25, N'DOGUBEYAZIT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (45, 4, 30, N'ELESKIRT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (46, 4, 35, N'HAMUR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (47, 4, 40, N'PATNOS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (48, 4, 45, N'TASLIÇAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (49, 4, 50, N'TUTAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (50, 5, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (51, 5, 20, N'GÖYNÜCEK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (52, 5, 25, N'GÜMÜSHACIKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (53, 5, 30, N'HAMAMÖZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (54, 5, 35, N'MERZIFON')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (55, 5, 40, N'SULUOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (56, 5, 45, N'TASOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (57, 6, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (58, 6, 20, N'AKYURT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (59, 6, 23, N'ALTINDAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (60, 6, 26, N'AYAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (61, 6, 29, N'BALA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (62, 6, 32, N'BEYPAZARI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (63, 6, 35, N'ÇAMLIDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (64, 6, 38, N'ÇANKAYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (65, 6, 41, N'ÇUBUK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (66, 6, 44, N'ELMADAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (67, 6, 47, N'ETIMESGUT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (68, 6, 50, N'EVREN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (69, 6, 53, N'GÖLBASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (70, 6, 56, N'GÜDÜL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (71, 6, 59, N'HAYMANA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (72, 6, 62, N'KALECIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (73, 6, 65, N'KAZAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (74, 6, 68, N'KEÇIÖREN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (75, 6, 71, N'KIZILCAHAMAM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (76, 6, 74, N'MAMAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (77, 6, 77, N'NALLIHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (78, 6, 80, N'POLATLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (79, 6, 83, N'SINCAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (80, 6, 86, N'SEREFLIKOÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (81, 6, 89, N'YENIMAHALLE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (82, 7, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (83, 7, 20, N'AKSEKI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (84, 7, 25, N'ALANYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (85, 7, 28, N'DEMRE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (86, 7, 30, N'ELMALI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (87, 7, 35, N'FINIKE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (88, 7, 40, N'GAZIPASA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (89, 7, 45, N'GÜNDOGMUS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (90, 7, 50, N'IBRADI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (91, 7, 55, N'KALE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (92, 7, 60, N'KAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (93, 7, 65, N'KEMER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (94, 7, 70, N'KORKUTELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (95, 7, 75, N'KUMLUCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (96, 7, 80, N'MANAVGAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (97, 7, 85, N'SERIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (98, 8, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (99, 8, 20, N'ARDANUÇ')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (100, 8, 25, N'ARHAVI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (101, 8, 30, N'BORÇKA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (102, 8, 32, N'HOPA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (103, 8, 35, N'MURGUL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (104, 8, 45, N'SAVSAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (105, 8, 50, N'YUSUFELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (106, 9, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (107, 9, 20, N'BOZDOGAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (108, 9, 24, N'BUHARKENT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (109, 9, 28, N'ÇINE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (110, 9, 32, N'GERMENCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (111, 9, 36, N'INCIRLIOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (112, 9, 40, N'KARACASU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (113, 9, 44, N'KARPUZLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (114, 9, 48, N'KOÇARLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (115, 9, 52, N'KÖSK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (116, 9, 56, N'KUSADASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (117, 9, 60, N'KUYUCAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (118, 9, 64, N'NAZILLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (119, 9, 68, N'SÖKE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (120, 9, 72, N'SULTANHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (121, 9, 76, N'YENIHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (122, 9, 80, N'YENIPAZAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (123, 10, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (124, 10, 20, N'AYVALIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (125, 10, 21, N'AKÇAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (126, 10, 24, N'BALYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (127, 10, 28, N'BANDIRMA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (128, 10, 32, N'BIGADIÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (129, 10, 36, N'BURHANIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (130, 10, 40, N'DURSUNBEY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (131, 10, 44, N'EDREMIT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (132, 10, 48, N'ERDEK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (133, 10, 52, N'GÖNEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (134, 10, 56, N'GÖMEÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (135, 10, 60, N'HAVRAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (136, 10, 64, N'IVRINDI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (137, 10, 68, N'KEPSUT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (138, 10, 72, N'MANYAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (139, 10, 76, N'MARMARA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (140, 10, 80, N'SAVASTEPE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (141, 10, 84, N'SINDIRGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (142, 10, 88, N'SUSURLUK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (143, 11, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (144, 11, 20, N'BOZÖYÜK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (145, 11, 25, N'GÖLPAZARI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (146, 11, 30, N'INHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (147, 11, 35, N'OSMANELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (148, 11, 40, N'PAZARYERI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (149, 11, 45, N'SÖGÜT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (150, 11, 50, N'YENIPAZAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (151, 12, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (152, 12, 20, N'ADAKLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (153, 12, 25, N'GENÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (154, 12, 30, N'KARLIOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (155, 12, 35, N'KIGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (156, 12, 40, N'SOLHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (157, 12, 45, N'YAYLADERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (158, 12, 55, N'YEDISU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (159, 13, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (160, 13, 20, N'ADILCEVAZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (161, 13, 25, N'AHLAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (162, 13, 30, N'GÜROYMAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (163, 13, 35, N'HIZAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (164, 13, 40, N'MUTKI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (165, 13, 45, N'TATVAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (166, 14, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (167, 14, 35, N'DÖRTDIVAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (168, 14, 45, N'GEREDE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (169, 14, 55, N'GÖYNÜK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (170, 14, 60, N'KIBRISCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (171, 14, 65, N'MENGEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (172, 14, 70, N'MUDURNU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (173, 14, 75, N'SEBEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (174, 14, 80, N'YENIÇAGA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (175, 15, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (176, 15, 20, N'ALTINYAYLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (177, 15, 25, N'AGLASUN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (178, 15, 30, N'BUCAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (179, 15, 35, N'ÇAVDIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (180, 15, 40, N'ÇELTIKÇI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (181, 15, 45, N'GÖLHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (182, 15, 50, N'KARAMANLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (183, 15, 55, N'KEMER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (184, 15, 60, N'TEFENNI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (185, 15, 65, N'YESILOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (186, 16, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (187, 16, 24, N'BÜYÜKORHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (188, 16, 28, N'GEMLIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (189, 16, 32, N'GÜRSU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (190, 16, 36, N'HARMANCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (191, 16, 40, N'INEGÖL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (192, 16, 44, N'IZNIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (193, 16, 48, N'KARACABEY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (194, 16, 52, N'KELES')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (195, 16, 56, N'KESTEL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (196, 16, 60, N'MUDANYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (197, 16, 64, N'MUSTAFAKEMA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (198, 16, 68, N'NILÜFER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (199, 16, 72, N'ORHANELI')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (200, 16, 76, N'ORHANGAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (201, 16, 80, N'OSMANGAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (202, 16, 84, N'YENISEHIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (203, 16, 88, N'YILDIRIM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (204, 17, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (205, 17, 25, N'AYVACIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (206, 17, 30, N'BAYRAMIÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (207, 17, 32, N'BOZCAADA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (208, 17, 35, N'BIGA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (209, 17, 40, N'ÇAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (210, 17, 45, N'ECEABAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (211, 17, 50, N'EZINE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (212, 17, 55, N'GELIBOLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (213, 17, 57, N'GÖKÇEADA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (214, 17, 65, N'LAPSEKI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (215, 17, 70, N'YENICE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (216, 18, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (217, 18, 25, N'ATKARACALAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (218, 18, 30, N'BAYRAMÖREN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (219, 18, 35, N'ÇERKES')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (220, 18, 40, N'ELDIVAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (221, 18, 50, N'ILGAZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (222, 18, 55, N'KIZILIRMAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (223, 18, 60, N'KORGUN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (224, 18, 65, N'KURSUNLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (225, 18, 70, N'ORTA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (226, 18, 75, N'OVACIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (227, 18, 80, N'SABANÖZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (228, 18, 85, N'YAPRAKLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (229, 19, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (230, 19, 25, N'ALACA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (231, 19, 30, N'BAYAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (232, 19, 35, N'BOGAZKALE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (233, 19, 40, N'DODURGA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (234, 19, 45, N'ISKILIP')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (235, 19, 50, N'KARGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (236, 19, 55, N'LAÇIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (237, 19, 60, N'MECITÖZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (238, 19, 65, N'OGUZLAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (239, 19, 70, N'ORTAKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (240, 19, 75, N'OSMANCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (241, 19, 80, N'SUNGURLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (242, 19, 85, N'UGURLUDAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (243, 20, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (244, 20, 23, N'ACIPAYAM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (245, 20, 26, N'AKKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (246, 20, 29, N'BABADAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (247, 20, 32, N'BAKLAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (248, 20, 35, N'BEKILLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (249, 20, 38, N'BEYAGAÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (250, 20, 41, N'BULDAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (251, 20, 44, N'BOZKURT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (252, 20, 47, N'ÇAL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (253, 20, 50, N'ÇAMELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (254, 20, 53, N'ÇARDAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (255, 20, 56, N'ÇIVRIL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (256, 20, 59, N'GÜNEY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (257, 20, 62, N'HONAZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (258, 20, 65, N'KALE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (259, 20, 68, N'SARAYKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (260, 20, 71, N'SERINHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (261, 20, 74, N'TAVAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (262, 21, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (263, 21, 25, N'BISMIL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (264, 21, 30, N'ÇERMIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (265, 21, 35, N'ÇINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (266, 21, 40, N'ÇÜNGÜS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (267, 21, 45, N'DICLE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (268, 21, 50, N'EGIL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (269, 21, 55, N'ERGANI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (270, 21, 60, N'HANI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (271, 21, 65, N'HAZRO')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (272, 21, 70, N'KOCAKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (273, 21, 75, N'KULP')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (274, 21, 80, N'LICE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (275, 21, 85, N'SILVAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (276, 22, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (277, 22, 25, N'ENEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (278, 22, 30, N'HAVSA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (279, 22, 35, N'IPSALA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (280, 22, 40, N'KESAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (281, 22, 45, N'LALAPASA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (282, 22, 50, N'MERIÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (283, 22, 55, N'SÜLOGLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (284, 22, 60, N'UZUNKÖPRÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (285, 23, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (286, 23, 25, N'AGIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (287, 23, 30, N'ALACAKAYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (288, 23, 35, N'ARICAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (289, 23, 40, N'BASKIL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (290, 23, 45, N'KARAKOÇAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (291, 23, 50, N'KEBAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (292, 23, 55, N'KOVANCILAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (293, 23, 60, N'MADEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (294, 23, 65, N'PALU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (295, 23, 70, N'SIVRICE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (296, 24, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (297, 24, 25, N'ÇAYIRLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (298, 24, 30, N'ILIÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (299, 24, 35, N'KEMAH')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (300, 24, 40, N'KEMALIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (301, 24, 45, N'OTLUKBELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (302, 24, 50, N'REFAHIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (303, 24, 55, N'TERCAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (304, 24, 60, N'ÜZÜMLÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (305, 25, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (306, 25, 23, N'ASKALE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (307, 25, 26, N'ÇAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (308, 25, 29, N'HINIS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (309, 25, 32, N'HORASAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (310, 25, 35, N'ILICA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (311, 25, 38, N'ISPIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (312, 25, 41, N'KARAÇOBAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (313, 25, 44, N'KARAYAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (314, 25, 47, N'KÖPRÜKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (315, 25, 50, N'NARMAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (316, 25, 53, N'OLTU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (317, 25, 55, N'OLUR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (318, 25, 58, N'PASINLER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (319, 25, 61, N'PAZARYOLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (320, 25, 64, N'SENKAYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (321, 25, 67, N'TEKMAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (322, 25, 70, N'TORTUM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (323, 25, 73, N'UZUNDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (324, 26, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (325, 26, 25, N'ALPU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (326, 26, 30, N'BEYLIKOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (327, 26, 35, N'ÇIFTELER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (328, 26, 40, N'GÜNYÜZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (329, 26, 45, N'HAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (330, 26, 50, N'INÖNÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (331, 26, 55, N'MAHMUDIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (332, 26, 60, N'MIHALGAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (333, 26, 65, N'MIHALIÇÇIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (334, 26, 70, N'SARICAKAYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (335, 26, 75, N'SEYITGAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (336, 26, 80, N'SIVRIHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (337, 27, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (338, 27, 25, N'ARABAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (339, 27, 30, N'ISLAHIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (340, 27, 35, N'KILIS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (341, 27, 40, N'KARGAMIS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (342, 27, 45, N'NIZIP')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (343, 27, 50, N'NURDAGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (344, 27, 55, N'OGUZELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (345, 27, 60, N'SAHINBEY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (346, 27, 65, N'SEHITKAMIL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (347, 27, 70, N'YAVUZELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (348, 28, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (349, 28, 25, N'ALUCRA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (350, 28, 30, N'BULANCAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (351, 28, 32, N'ÇAMOLUK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (352, 28, 35, N'ÇANAKÇI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (353, 28, 40, N'DERELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (354, 28, 42, N'DOGANKENT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (355, 28, 45, N'ESPIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (356, 28, 50, N'EYNESIL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (357, 28, 55, N'GÖRELE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (358, 28, 60, N'GÜCE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (359, 28, 65, N'KESAP')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (360, 28, 70, N'PIRAZIZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (361, 28, 75, N'SEBINKARAHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (362, 28, 80, N'TIREBOLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (363, 28, 85, N'YAGLIDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (364, 29, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (365, 29, 25, N'KELKIT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (366, 29, 30, N'KÖSE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (367, 29, 35, N'KÜRTÜN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (368, 29, 40, N'SIRAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (369, 29, 45, N'TORUL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (370, 30, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (371, 30, 25, N'ÇUKURCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (372, 30, 30, N'SEMDINLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (373, 30, 35, N'YÜKSEKOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (374, 31, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (375, 31, 25, N'ALTINÖZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (376, 31, 30, N'BELEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (377, 31, 35, N'DÖRTYOL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (378, 31, 40, N'ERZIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (379, 31, 45, N'HASSA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (380, 31, 50, N'ISKENDERUN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (381, 31, 55, N'KIRIKHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (382, 31, 60, N'KUMLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (383, 31, 65, N'REYHANLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (384, 31, 70, N'SAMANDAGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (385, 31, 75, N'YAYLADAGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (386, 32, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (387, 32, 25, N'AKSU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (388, 32, 30, N'ATABEY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (389, 32, 35, N'EGIRDIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (390, 32, 40, N'GELENDOST')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (391, 32, 45, N'GÖNEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (392, 32, 50, N'KEÇIBORLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (393, 32, 55, N'SENIRKENT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (394, 32, 60, N'SÜTÇÜLER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (395, 32, 65, N'SARKIKARAAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (396, 32, 70, N'ULUBORLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (397, 32, 75, N'YENISARBADE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (398, 32, 80, N'YALVAÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (399, 33, 0, N'MERKEZ')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (400, 33, 25, N'ANAMUR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (401, 33, 30, N'AYDINCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (402, 33, 35, N'BOZYAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (403, 33, 40, N'ÇAMLIYAYLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (404, 33, 45, N'ERDEMLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (405, 33, 50, N'GÜLNAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (406, 33, 55, N'MUT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (407, 33, 60, N'SILIFKE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (408, 33, 65, N'TARSUS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (409, 34, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (410, 34, 23, N'ADALAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (411, 34, 24, N'AVCILAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (412, 34, 25, N'BAGCILAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (413, 34, 26, N'BAKIRKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (414, 34, 27, N'BAHÇELIEVLE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (415, 34, 29, N'BAYRAMPASA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (416, 34, 32, N'BESIKTAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (417, 34, 35, N'BEYKOZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (418, 34, 38, N'BEYOGLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (419, 34, 41, N'BÜYÜKÇEKMECE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (420, 34, 44, N'ÇATALCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (421, 34, 47, N'EMINÖNÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (422, 34, 50, N'EYÜP')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (423, 34, 51, N'ESENLER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (424, 34, 53, N'FATIH')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (425, 34, 56, N'GAZIOSMANPASA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (426, 34, 57, N'GÜNGÖREN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (427, 34, 59, N'KADIKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (428, 34, 61, N'KAGITHANE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (429, 34, 64, N'KARTAL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (430, 34, 67, N'KÜÇÜKÇEKMECE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (431, 34, 68, N'MALTEPE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (432, 34, 70, N'PENDIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (433, 34, 73, N'SARIYER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (434, 34, 76, N'SILIVRI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (435, 34, 77, N'SULTANBEYLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (436, 34, 79, N'SILE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (437, 34, 81, N'SISLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (438, 34, 82, N'TUZLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (439, 34, 84, N'ÜMRANIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (440, 34, 87, N'ÜSKÜDAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (441, 34, 90, N'YALOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (442, 34, 93, N'ZEYTINBURNU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (443, 35, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (444, 35, 23, N'ALIAGA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (445, 35, 26, N'BAYINDIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (446, 35, 27, N'BALÇOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (447, 35, 29, N'BERGAMA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (448, 35, 32, N'BEYDAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (449, 35, 35, N'BORNOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (450, 35, 37, N'BUCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (451, 35, 41, N'ÇESME')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (452, 35, 42, N'ÇIGLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (453, 35, 44, N'DIKILI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (454, 35, 47, N'FOÇA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (455, 35, 48, N'GAZIEMIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (456, 35, 49, N'GÜZELBAHÇE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (457, 35, 50, N'KARABURUN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (458, 35, 53, N'KARSIYAKA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (459, 35, 56, N'KEMALPASA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (460, 35, 59, N'KINIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (461, 35, 62, N'KIRAZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (462, 35, 65, N'KONAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (463, 35, 68, N'MENDERES')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (464, 35, 71, N'MENEMEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (465, 35, 72, N'NARLIDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (466, 35, 74, N'ÖDEMIS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (467, 35, 77, N'SEFERIHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (468, 35, 80, N'SELÇUK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (469, 35, 83, N'TIRE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (470, 35, 86, N'TORBALI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (471, 35, 89, N'URLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (472, 36, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (473, 36, 24, N'AKYAKA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (474, 36, 36, N'ARPAÇAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (475, 36, 44, N'DIGOR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (476, 36, 60, N'KAGIZMAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (477, 36, 68, N'SARIKAMIS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (478, 36, 72, N'SELIM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (479, 36, 76, N'SUSUZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (480, 37, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (481, 37, 23, N'ABANA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (482, 37, 26, N'AGLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (483, 37, 29, N'ARAÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (484, 37, 32, N'AZDAVAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (485, 37, 35, N'BOZKURT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (486, 37, 38, N'CIDE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (487, 37, 41, N'ÇATALZEYTIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (488, 37, 44, N'DADAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (489, 37, 47, N'DEVREKANI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (490, 37, 50, N'DOGANYURT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (491, 37, 53, N'HANÖNÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (492, 37, 56, N'IHSANGAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (493, 37, 59, N'INEBOLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (494, 37, 62, N'KÜRE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (495, 37, 65, N'PINARBASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (496, 37, 68, N'SEYDILER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (497, 37, 71, N'SENPAZAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (498, 37, 74, N'TASKÖPRÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (499, 37, 77, N'TOSYA')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (500, 38, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (501, 38, 24, N'AKKISLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (502, 38, 28, N'BÜNYAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (503, 38, 32, N'DEVELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (504, 38, 36, N'FELAHIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (505, 38, 40, N'HACILAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (506, 38, 44, N'INCESU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (507, 38, 48, N'KOCASINAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (508, 38, 52, N'MELIKGAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (509, 38, 56, N'ÖZVATAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (510, 38, 60, N'PINARBASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (511, 38, 64, N'SARIOGLAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (512, 38, 68, N'SARIZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (513, 38, 72, N'TALAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (514, 38, 76, N'TOMARZA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (515, 38, 80, N'YAHYALI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (516, 38, 84, N'YESILHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (517, 39, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (518, 39, 25, N'BABAESKI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (519, 39, 30, N'DEMIRKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (520, 39, 35, N'KOFÇAZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (521, 39, 40, N'LÜLEBURGAZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (522, 39, 45, N'PEHLIVANKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (523, 39, 50, N'PINARHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (524, 39, 55, N'VIZE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (525, 40, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (526, 40, 20, N'AKÇAKENT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (527, 40, 25, N'AKPINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (528, 40, 27, N'BOZTEPE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (529, 40, 30, N'ÇIÇEKDAGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (530, 40, 35, N'KAMAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (531, 40, 40, N'MUCUR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (532, 41, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (533, 41, 20, N'DARICA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (534, 41, 25, N'GEBZE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (535, 41, 30, N'GÖLCÜK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (536, 41, 35, N'KANDIRA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (537, 41, 40, N'KARAMÜRSEL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (538, 41, 45, N'KÖRFEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (539, 42, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (540, 42, 22, N'AHIRLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (541, 42, 24, N'AKÖREN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (542, 42, 26, N'AKSEHIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (543, 42, 28, N'ALTINEKIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (544, 42, 30, N'BEYSEHIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (545, 42, 32, N'BOZKIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (546, 42, 34, N'DEREBUCAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (547, 42, 36, N'CIHANBEYLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (548, 42, 38, N'ÇUMRA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (549, 42, 40, N'ÇELTIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (550, 42, 42, N'DERBENT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (551, 42, 44, N'DOGANHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (552, 42, 46, N'EMIRGAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (553, 42, 48, N'EREGLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (554, 42, 50, N'GÜNEYSINIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (555, 42, 52, N'HALKAPINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (556, 42, 54, N'HADIM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (557, 42, 55, N'HALKAPINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (558, 42, 56, N'HÜYÜK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (559, 42, 58, N'ILGIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (560, 42, 60, N'KADINHANI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (561, 42, 62, N'KARAPINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (562, 42, 64, N'KARATAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (563, 42, 66, N'KULU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (564, 42, 68, N'MERAM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (565, 42, 70, N'SARAYÖNÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (566, 42, 72, N'SELÇUKLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (567, 42, 74, N'SEYDISEHIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (568, 42, 76, N'TASKENT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (569, 42, 78, N'TUZLUKÇU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (570, 42, 80, N'YALIHÖYÜK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (571, 42, 82, N'YUNAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (572, 43, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (573, 43, 25, N'ALTINTAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (574, 43, 30, N'ASLANAPA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (575, 43, 35, N'CAVDARHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (576, 43, 40, N'DOMANIÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (577, 43, 45, N'DUMLUPINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (578, 43, 50, N'EMET')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (579, 43, 55, N'GEDIZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (580, 43, 60, N'HISARCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (581, 43, 65, N'PAZARLAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (582, 43, 70, N'SIMAV')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (583, 43, 75, N'SAPHANE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (584, 43, 80, N'TAVSANLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (585, 44, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (586, 44, 25, N'AKÇADAG')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (587, 44, 30, N'ARAPGIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (588, 44, 35, N'ARGUVAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (589, 44, 40, N'BATTALGAZI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (590, 44, 45, N'DARENDE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (591, 44, 50, N'DOGANSEHIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (592, 44, 55, N'DOGANYOL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (593, 44, 60, N'HEKIMHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (594, 44, 65, N'KALE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (595, 44, 70, N'KULUNCAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (596, 44, 75, N'PÖTÜRGE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (597, 44, 80, N'YAZIHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (598, 44, 85, N'YESILYURT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (599, 45, 0, N'MERKEZ')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (600, 45, 25, N'AHMETLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (601, 45, 30, N'AKHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (602, 45, 35, N'ALASEHIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (603, 45, 40, N'DEMIRCI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (604, 45, 45, N'GÖLMARMARA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (605, 45, 50, N'GÖRDES')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (606, 45, 55, N'KIRKAGAÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (607, 45, 60, N'KÖPRÜBASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (608, 45, 65, N'KULA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (609, 45, 70, N'SALIHLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (610, 45, 75, N'SARIGÖL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (611, 45, 80, N'SARUHANLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (612, 45, 85, N'SELENDI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (613, 45, 90, N'SOMA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (614, 45, 95, N'TURGUTLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (615, 46, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (616, 46, 25, N'AFSIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (617, 46, 30, N'ANDIRIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (618, 46, 35, N'ÇAGLAYANCER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (619, 46, 40, N'EKINÖZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (620, 46, 45, N'ELBISTAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (621, 46, 50, N'GÖKSUN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (622, 46, 55, N'NURHAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (623, 46, 60, N'PAZARCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (624, 46, 65, N'TÜRKOGLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (625, 47, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (626, 47, 25, N'DARGEÇIT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (627, 47, 30, N'DERIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (628, 47, 40, N'KIZILTEPE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (629, 47, 45, N'MAZIDAGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (630, 47, 50, N'MIDYAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (631, 47, 55, N'NUSAYBIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (632, 47, 60, N'ÖMERLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (633, 47, 65, N'SAVUR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (634, 47, 70, N'YESILLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (635, 48, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (636, 48, 25, N'BODRUM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (637, 48, 30, N'DALAMAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (638, 48, 35, N'DATÇA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (639, 48, 40, N'FETHIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (640, 48, 45, N'KAVAKLIDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (641, 48, 50, N'KÖYCEGIZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (642, 48, 55, N'MARMARIS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (643, 48, 60, N'MILAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (644, 48, 65, N'ORTACA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (645, 48, 70, N'ULA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (646, 48, 75, N'YATAGAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (647, 49, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (648, 49, 25, N'BULANIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (649, 49, 30, N'HASKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (650, 49, 35, N'KORKUT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (651, 49, 40, N'MALAZGIRT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (652, 49, 45, N'VARTO')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (653, 50, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (654, 50, 25, N'ACIGÖL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (655, 50, 30, N'AVANOS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (656, 50, 35, N'DERINKUYU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (657, 50, 40, N'GÜLSEHIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (658, 50, 45, N'HACIBEKTAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (659, 50, 50, N'KOZAKLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (660, 50, 55, N'ÜRGÜP')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (661, 51, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (662, 51, 25, N'ALTUNHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (663, 51, 30, N'BOR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (664, 51, 35, N'ÇAMARDI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (665, 51, 40, N'ÇIFTLIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (666, 51, 45, N'ULUKISLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (667, 52, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (668, 52, 23, N'AKKUS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (669, 52, 26, N'AYBASTI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (670, 52, 29, N'ÇAMAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (671, 52, 32, N'ÇATALPINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (672, 52, 35, N'ÇAYBASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (673, 52, 38, N'FATSA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (674, 52, 41, N'GÖLKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (675, 52, 44, N'GÖLYALI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (676, 52, 47, N'GÜRGENTEPE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (677, 52, 50, N'IKIZCE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (678, 52, 53, N'KORGAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (679, 52, 56, N'KABADÜZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (680, 52, 59, N'KABATAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (681, 52, 62, N'KUMRU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (682, 52, 65, N'MESUDIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (683, 52, 68, N'PERSEMBE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (684, 52, 71, N'ULUBEY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (685, 52, 74, N'ÜNYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (686, 53, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (687, 53, 25, N'ARDESEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (688, 53, 30, N'ÇAMLIHEMSIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (689, 53, 35, N'ÇAYELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (690, 53, 40, N'DEREPAZARI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (691, 53, 45, N'FINDIKLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (692, 53, 50, N'GÜNEYSU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (693, 53, 55, N'HEMSIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (694, 53, 60, N'IKIZDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (695, 53, 65, N'IYIDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (696, 53, 70, N'KALKANDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (697, 53, 75, N'PAZAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (698, 54, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (699, 54, 25, N'AKYAZI')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (700, 54, 30, N'FERIZLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (701, 54, 35, N'GEYVE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (702, 54, 40, N'HENDEK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (703, 54, 45, N'KARAPÜRÇEK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (704, 54, 50, N'KARASU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (705, 54, 55, N'KAYNARCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (706, 54, 60, N'KOCAALI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (707, 54, 65, N'PAMUKOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (708, 54, 70, N'SAPANCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (709, 54, 75, N'SÖGÜTLÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (710, 54, 80, N'TARAKLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (711, 55, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (712, 55, 25, N'ALAÇAM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (713, 55, 30, N'ASARCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (714, 55, 35, N'AYVACIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (715, 55, 40, N'BAFRA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (716, 55, 45, N'ÇARSAMBA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (717, 55, 50, N'HAVZA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (718, 55, 55, N'KAVAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (719, 55, 60, N'LADIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (720, 55, 65, N'19MAYIS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (721, 55, 70, N'SALIPAZARI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (722, 55, 75, N'TEKKEKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (723, 55, 80, N'TERME')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (724, 55, 85, N'VEZIRKÖPRÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (725, 55, 90, N'YAKAKENT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (726, 56, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (727, 56, 25, N'AYDINLAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (728, 56, 30, N'BAYKAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (729, 56, 35, N'ERUH')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (730, 56, 42, N'KOZLUK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (731, 56, 45, N'KURTALAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (732, 56, 50, N'PERVARI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (733, 56, 55, N'SIRVAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (734, 57, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (735, 57, 25, N'AYANCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (736, 57, 30, N'BOYABAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (737, 57, 35, N'DIKMEN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (738, 57, 40, N'DURAGAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (739, 57, 45, N'ERFELEK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (740, 57, 50, N'GERZE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (741, 57, 55, N'SARAYDÜZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (742, 57, 60, N'TÜRKELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (743, 58, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (744, 58, 24, N'AKINCILAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (745, 58, 28, N'ALTINYAYLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (746, 58, 32, N'DIVRIGI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (747, 58, 36, N'DOGANSAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (748, 58, 40, N'GEMEREK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (749, 58, 44, N'GÖLOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (750, 58, 48, N'GÜRÜN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (751, 58, 52, N'HAFIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (752, 58, 56, N'IMRANLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (753, 58, 60, N'KANGAL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (754, 58, 64, N'KOYULHISAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (755, 58, 68, N'SUSEHRI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (756, 58, 72, N'SARKISLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (757, 58, 76, N'ULAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (758, 58, 80, N'YILDIZELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (759, 58, 84, N'ZARA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (760, 59, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (761, 59, 25, N'ÇERKEZKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (762, 59, 30, N'ÇORLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (763, 59, 35, N'HAYRABOLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (764, 59, 40, N'MALKARA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (765, 59, 45, N'MARMARAEREGLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (766, 59, 50, N'MURATLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (767, 59, 55, N'SARAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (768, 59, 60, N'SARKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (769, 60, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (770, 60, 25, N'ALMUS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (771, 60, 30, N'ARTOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (772, 60, 35, N'BASÇIFTLIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (773, 60, 40, N'ERBAA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (774, 60, 45, N'NIKSAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (775, 60, 50, N'PAZAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (776, 60, 55, N'RESADIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (777, 60, 60, N'SULUSARAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (778, 60, 65, N'TURHAL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (779, 60, 70, N'YESILYURT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (780, 60, 75, N'ZILE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (781, 61, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (782, 61, 24, N'AKÇAABAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (783, 61, 28, N'ARAKLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (784, 61, 32, N'ARSIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (785, 61, 36, N'BESIKDÜZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (786, 61, 40, N'ÇARSIBASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (787, 61, 44, N'ÇAYKARA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (788, 61, 48, N'DERNEKPAZAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (789, 61, 52, N'DÜZKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (790, 61, 56, N'HAYRAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (791, 61, 60, N'KÖPRÜBASI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (792, 61, 64, N'MAÇKA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (793, 61, 68, N'OF')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (794, 61, 72, N'SÜRMENE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (795, 61, 76, N'SALPAZARI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (796, 61, 80, N'TONYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (797, 61, 84, N'VAKFIKEBIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (798, 61, 88, N'YOMRA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (799, 62, 0, N'MERKEZ')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (800, 62, 25, N'ÇEMISGEZEK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (801, 62, 30, N'HOZAT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (802, 62, 35, N'MAZGIRT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (803, 62, 40, N'NAZIMIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (804, 62, 45, N'OVACIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (805, 62, 50, N'PERTEK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (806, 62, 55, N'PÜLÜMÜR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (807, 63, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (808, 63, 25, N'AKÇAKALE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (809, 63, 30, N'BIRECIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (810, 63, 35, N'BOZOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (811, 63, 40, N'CEYLANPINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (812, 63, 45, N'HALFETI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (813, 63, 50, N'HARRAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (814, 63, 55, N'HILVAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (815, 63, 60, N'SIVEREK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (816, 63, 65, N'SURUÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (817, 63, 70, N'VIRANSEHIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (818, 64, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (819, 64, 25, N'BANAZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (820, 64, 30, N'ESME')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (821, 64, 35, N'KARAHALLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (822, 64, 40, N'SIVASLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (823, 64, 45, N'ULUBEY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (824, 65, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (825, 65, 25, N'BAHÇESARAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (826, 65, 30, N'BASKALE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (827, 65, 35, N'ÇALDIRAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (828, 65, 40, N'ÇATAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (829, 65, 45, N'EDREMIT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (830, 65, 50, N'ERCIS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (831, 65, 55, N'GEVAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (832, 65, 60, N'GÜRPINAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (833, 65, 65, N'MURADIYE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (834, 65, 70, N'ÖZALP')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (835, 65, 75, N'SARAY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (836, 66, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (837, 66, 25, N'AKDAGMADENI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (838, 66, 30, N'AYDINCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (839, 66, 35, N'BOGAZLIYAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (840, 66, 40, N'ÇANDIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (841, 66, 45, N'ÇAYIRALAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (842, 66, 50, N'ÇEKEREK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (843, 66, 55, N'KADISEHRI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (844, 66, 60, N'SARIKAYA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (845, 66, 65, N'SARAYKENT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (846, 66, 70, N'SORGUN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (847, 66, 75, N'SEFAATLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (848, 66, 80, N'YENIFAKILI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (849, 66, 85, N'YERKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (850, 67, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (851, 67, 24, N'ALAPLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (852, 67, 36, N'ÇAMOLUK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (853, 67, 38, N'ÇAYCUMA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (854, 67, 44, N'DEVREK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (855, 67, 52, N'EFLANI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (856, 67, 56, N'EREGLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (857, 67, 60, N'GÖKÇEBEY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (858, 68, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (859, 68, 25, N'AGAÇÖREN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (860, 68, 30, N'ESKIL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (861, 68, 35, N'GÜLAGAÇ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (862, 68, 40, N'GÜZELYURT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (863, 68, 45, N'ORTAKÖY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (864, 68, 50, N'SARIYAHSI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (865, 69, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (866, 69, 25, N'AYDINTEPE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (867, 69, 30, N'DEMIRÖZÜ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (868, 70, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (869, 70, 25, N'AYRANCI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (870, 70, 30, N'BASYAYLA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (871, 70, 35, N'ERMENEK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (872, 70, 40, N'KAZIMKARABEKIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (873, 70, 45, N'SARIVELILER')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (874, 71, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (875, 71, 25, N'BAHSILI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (876, 71, 30, N'BAGLISEYH')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (877, 71, 35, N'ÇELEBI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (878, 71, 40, N'DELICE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (879, 71, 45, N'KARAKEÇILI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (880, 71, 50, N'KESKIN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (881, 71, 55, N'SULAKYURT')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (882, 71, 60, N'YAHSIHAN')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (883, 72, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (884, 72, 25, N'GERCÜS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (885, 72, 30, N'HASANKEYF')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (886, 72, 35, N'BESIRI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (887, 72, 37, N'KOZLUK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (888, 72, 40, N'SASON')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (889, 73, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (890, 73, 25, N'BEYTÜSSEBA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (891, 73, 30, N'ULUDERE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (892, 73, 35, N'CIZRE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (893, 73, 40, N'IDIL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (894, 73, 45, N'SILOPI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (895, 73, 55, N'GÜÇLÜKONAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (896, 74, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (897, 74, 20, N'AMASRA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (898, 74, 30, N'KURUCASILE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (899, 74, 40, N'ULUS')
GO
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (900, 75, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (901, 75, 30, N'ÇILDIR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (902, 75, 35, N'DAMAL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (903, 75, 50, N'GÖLE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (904, 75, 55, N'HANAK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (905, 75, 75, N'POSOF')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (906, 76, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (907, 76, 25, N'ARALIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (908, 76, 50, N'KARAKOYUNLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (909, 76, 75, N'TUZLUCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (910, 77, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (911, 77, 10, N'ALTINOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (912, 77, 15, N'ARMUTLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (913, 77, 20, N'CINARCIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (914, 77, 22, N'CIFTLIKKOY')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (915, 77, 80, N'TERMAL')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (916, 78, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (917, 78, 30, N'EFLANI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (918, 78, 33, N'ESKIPAZAR')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (919, 78, 50, N'OVACIK')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (920, 78, 70, N'SAFRANBOLU')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (921, 78, 90, N'YENICE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (922, 79, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (923, 79, 30, N'ELBEYLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (924, 79, 50, N'MUSABEYLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (925, 79, 60, N'POLATELI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (926, 80, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (927, 80, 20, N'BAHÇE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (928, 80, 23, N'HASANBEYLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (929, 80, 25, N'DÜZIÇI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (930, 80, 30, N'KADIRLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (931, 80, 45, N'SUNBAS')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (932, 80, 50, N'TOPRAKKALE')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (933, 81, 0, N'MERKEZ')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (934, 81, 20, N'AKÇAKOCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (935, 81, 25, N'CUMAYERI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (936, 81, 30, N'ÇILIMLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (937, 81, 35, N'GÖLYAKA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (938, 81, 40, N'GÜMÜSOVA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (939, 81, 45, N'KAYNASLI')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (940, 81, 50, N'YIGILCA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (941, 3, 0, N'SINANPASA')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (942, 9, 0, N'DIDIM')
INSERT [dbo].[ilce] ([ID], [ilKodu], [ilceKodu], [ilceAdi]) VALUES (943, 41, 0, N'DERINCE')
SET IDENTITY_INSERT [dbo].[ilce] OFF
ALTER TABLE [dbo].[adresler] ADD  CONSTRAINT [DF_adresler_gecicimi]  DEFAULT ((1)) FOR [gecicimi]
GO
ALTER TABLE [dbo].[kategoriler] ADD  CONSTRAINT [DF_kategoriler_anakategoriID]  DEFAULT ((0)) FOR [anakategoriID]
GO
ALTER TABLE [dbo].[siparisler] ADD  CONSTRAINT [DF_siparisler_siparisSaticiOnayi]  DEFAULT ((0)) FOR [siparisSaticiOnayi]
GO
ALTER TABLE [dbo].[siparisler] ADD  CONSTRAINT [DF_siparisler_siparisAliciOnayi]  DEFAULT ((0)) FOR [siparisAliciOnayi]
GO
ALTER TABLE [dbo].[uyeler] ADD  CONSTRAINT [DF_uyeler_bakiye]  DEFAULT ((0)) FOR [bakiye]
GO
ALTER TABLE [dbo].[adresler]  WITH CHECK ADD  CONSTRAINT [FK_adresler_il1] FOREIGN KEY([sehirID])
REFERENCES [dbo].[il] ([ID])
GO
ALTER TABLE [dbo].[adresler] CHECK CONSTRAINT [FK_adresler_il1]
GO
ALTER TABLE [dbo].[adresler]  WITH CHECK ADD  CONSTRAINT [FK_adresler_ilce1] FOREIGN KEY([ilceID])
REFERENCES [dbo].[ilce] ([ID])
GO
ALTER TABLE [dbo].[adresler] CHECK CONSTRAINT [FK_adresler_ilce1]
GO
ALTER TABLE [dbo].[adresler]  WITH CHECK ADD  CONSTRAINT [FK_adresler_uyeler] FOREIGN KEY([uyeID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[adresler] CHECK CONSTRAINT [FK_adresler_uyeler]
GO
ALTER TABLE [dbo].[fiyatAlarm]  WITH CHECK ADD  CONSTRAINT [FK_fiyatAlarm_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[fiyatAlarm] CHECK CONSTRAINT [FK_fiyatAlarm_urunler1]
GO
ALTER TABLE [dbo].[fiyatAlarm]  WITH CHECK ADD  CONSTRAINT [FK_fiyatAlarm_uyeler1] FOREIGN KEY([uyeID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[fiyatAlarm] CHECK CONSTRAINT [FK_fiyatAlarm_uyeler1]
GO
ALTER TABLE [dbo].[ilce]  WITH CHECK ADD  CONSTRAINT [FK_ilce_il] FOREIGN KEY([ilKodu])
REFERENCES [dbo].[il] ([ID])
GO
ALTER TABLE [dbo].[ilce] CHECK CONSTRAINT [FK_ilce_il]
GO
ALTER TABLE [dbo].[kategoriOzellikleri]  WITH CHECK ADD  CONSTRAINT [FK_kategoriOzellikleri_kategoriler1] FOREIGN KEY([kategoriID])
REFERENCES [dbo].[kategoriler] ([kategoriID])
GO
ALTER TABLE [dbo].[kategoriOzellikleri] CHECK CONSTRAINT [FK_kategoriOzellikleri_kategoriler1]
GO
ALTER TABLE [dbo].[sepet]  WITH CHECK ADD  CONSTRAINT [FK_sepet_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[sepet] CHECK CONSTRAINT [FK_sepet_urunler1]
GO
ALTER TABLE [dbo].[sepet]  WITH CHECK ADD  CONSTRAINT [FK_sepet_uyeler1] FOREIGN KEY([uyeID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[sepet] CHECK CONSTRAINT [FK_sepet_uyeler1]
GO
ALTER TABLE [dbo].[siparisler]  WITH CHECK ADD  CONSTRAINT [FK_siparisler_urunler] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[siparisler] CHECK CONSTRAINT [FK_siparisler_urunler]
GO
ALTER TABLE [dbo].[siparisler]  WITH CHECK ADD  CONSTRAINT [FK_siparisler_uyeler] FOREIGN KEY([saticiID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[siparisler] CHECK CONSTRAINT [FK_siparisler_uyeler]
GO
ALTER TABLE [dbo].[siparisler]  WITH CHECK ADD  CONSTRAINT [FK_siparisler_uyeler1] FOREIGN KEY([aliciID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[siparisler] CHECK CONSTRAINT [FK_siparisler_uyeler1]
GO
ALTER TABLE [dbo].[urunKampanyalar]  WITH CHECK ADD  CONSTRAINT [FK_urunKampanyalar_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[urunKampanyalar] CHECK CONSTRAINT [FK_urunKampanyalar_urunler1]
GO
ALTER TABLE [dbo].[urunler]  WITH CHECK ADD  CONSTRAINT [FK_urunler_kategoriler] FOREIGN KEY([kategoriID])
REFERENCES [dbo].[kategoriler] ([kategoriID])
GO
ALTER TABLE [dbo].[urunler] CHECK CONSTRAINT [FK_urunler_kategoriler]
GO
ALTER TABLE [dbo].[urunler]  WITH CHECK ADD  CONSTRAINT [FK_urunler_markalar] FOREIGN KEY([markaID])
REFERENCES [dbo].[markalar] ([markaID])
GO
ALTER TABLE [dbo].[urunler] CHECK CONSTRAINT [FK_urunler_markalar]
GO
ALTER TABLE [dbo].[urunler]  WITH CHECK ADD  CONSTRAINT [FK_urunler_uyeler] FOREIGN KEY([saticiID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[urunler] CHECK CONSTRAINT [FK_urunler_uyeler]
GO
ALTER TABLE [dbo].[urunOzellikleri]  WITH CHECK ADD  CONSTRAINT [FK_urunOzellikleri_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[urunOzellikleri] CHECK CONSTRAINT [FK_urunOzellikleri_urunler1]
GO
ALTER TABLE [dbo].[urunResimler]  WITH CHECK ADD  CONSTRAINT [FK_urunResimler_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[urunResimler] CHECK CONSTRAINT [FK_urunResimler_urunler1]
GO
