﻿using System.Web;
using System.Web.Optimization;

namespace b141210041_MusaDemir
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
          
          
            bundles.Add(new ScriptBundle("~/bundles/mainjs").Include(
                /*
                  <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>*/
                "~/Scripts/jquery.js",
                 "~/Scripts/bootstrap.min.js",
                  "~/Scripts/jquery.scrollUp.min.js",
                   "~/Scripts/price-range.js",
                    "~/Scripts/jquery.prettyPhoto.js",
                     "~/Scripts/main.js"
                      
                ));

            /*
              <link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">*/
            bundles.Add(new StyleBundle("~/Content/mainstyles").Include(
                      "~/Content/bootstrap.min.css",
                       "~/Content/font-awesome.min.css",
                        "~/Content/prettyPhoto.css",
                         "~/Content/price-range.css",
                          "~/Content/animate.css",
                           "~/Content/main.css",
                            "~/Content/responsive.css"
                            ));
        }
    }
}