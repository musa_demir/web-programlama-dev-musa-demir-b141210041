﻿using System.Web;
using System.Web.Mvc;

namespace b141210041_MusaDemir
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
