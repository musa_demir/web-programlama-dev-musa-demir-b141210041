﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(b141210041_MusaDemir.Startup))]
namespace b141210041_MusaDemir
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
